use Rack::Static,
  :urls => ["/images", "/javascripts", "/stylesheets"],
  :root => "public"

run lambda { |env|
  [
    200,
    {
      'Content-Type'  => 'text/html',
      'Cache-Control' => 'public, max-age=86400'
    },
    File.open(set_path(env), File::RDONLY)
  ]
}

def set_path(env)
  if env["PATH_INFO"] == "/"
    'public/index.html'
  else
    'public' + env["PATH_INFO"]
  end
end